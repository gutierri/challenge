import unittest

from src import rss2json


class TestFeedToJson(unittest.TestCase):
    def setUp(self):
        fixture_feed = r'test/fixtures/feed.xml'
        self.feed = rss2json.FeedToJson(fixture_feed)
        self.feed_item = self.feed.connect_exservice().entries[0]['summary']

    def test_get_feed_items(self):
        self.assertIsInstance(self.feed.feed_items(), dict)
        self.assertIn('feed', self.feed.feed_items().keys())
        self.assertIsInstance(self.feed.feed_items()['feed'], list)

    def test_feed_parser_item(self):

        spec_feed_item = {'link': 'https://domain.tld/path-mag',
                          'title': 'feed title',
                          'summary': '<p>paragraph</p>'}
        spec_keys = ['title', 'link', 'content']
        parser_item = self.feed.parse_item(spec_feed_item)

        self.assertCountEqual(parser_item.keys(), spec_keys)

        self.assertEqual(parser_item['title'], 'feed title')
        self.assertEqual(parser_item['link'], 'https://domain.tld/path-mag')

    def test_feed_parser_content_item(self):
        feed_item_content = '''
        <p>foobar</p>
        <p>&nbsp;</p>
        <div><img src="https://domain.tld/path.png" /></div>
        <div><ul>
            <li><a href="https://domain.tld/links">link</a></li>
            <li><a href="https://domain.tld/links">link</a></li>
            <li>no-link</li>
            </ul></div>
        <p>\n\t<strong>Dimens&otilde;es</strong></p>\n
        <p>\n\tComprimento:2,57 m</p>
        '''
        parser_item = self.feed._filter_content(feed_item_content)

        self.assertEqual(len(parser_item), 5)

        self.assertEqual(parser_item[0]['content'], 'foobar')
        self.assertEqual(parser_item[0]['type'], 'text')

        self.assertEqual(parser_item[1]['type'], 'image')
        self.assertEqual(parser_item[1]['content'],
                         'https://domain.tld/path.png')

        self.assertEqual(parser_item[2]['type'], 'links')
        self.assertEqual(parser_item[2]['content'],
                         ['https://domain.tld/links',
                          'https://domain.tld/links'])

        self.assertEqual(parser_item[3]['content'], 'Dimensões')
        self.assertEqual(parser_item[3]['type'], 'text')

        self.assertEqual(parser_item[4]['content'], 'Comprimento:2,57 m')
        self.assertEqual(parser_item[4]['type'], 'text')
