import json
import unittest

from src import webservice


class TestWebService(unittest.TestCase):
    def setUp(self):
        webservice.app.testing = True
        self.app = webservice.app.test_client()

    def test_jwt_gentoken(self):
        auth = self.app.post('/auth', data=json.dumps({'username': 'test',
                                                       'password': 'test'}),
                             content_type='application/json')
        self.assertEqual(auth.status_code, 200)

        auth = self.app.post('/auth', data=json.dumps({'username': 'foo',
                                                       'password': 'foo'}),
                             content_type='application/json')
        self.assertEqual(auth.status_code, 401)

    def test_view_feed_items(self):
        tpl_auth = 'Bearer {access_token}'

        self.assertEqual(self.app.get('/').status_code, 401)
        request_token = self.app.post('/auth',
                                      data=json.dumps({'username': 'test',
                                                       'password': 'test'}),
                                      content_type='application/json')

        key = tpl_auth.format(access_token=request_token.json['access_token'])
        self.assertEqual(self.app.get('/',
                         headers={'Authorization': key}).status_code, 200)
