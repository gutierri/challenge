deps/technologies
=================
- Docker
- Docker Composer

or

- Python >= 3.5 
   - (webservice) Flask
- Tox

Running and available commands
==============================

Running webservice

.. code-block:: bash

   $ make webservice # call docker-compose run webservice, with dev envs default

Running output feed to JSON std

.. code-block:: bash

   $ make feed 

or

.. code-block:: bash

   $ python -m "src.rss2json" 

Running suit tests

.. code-block:: bash

   $ make tests

Running all suit tests (with TOX)

.. code-block:: bash

   $ make all-tests

or

.. code-block:: bash

   $ tox

Extra commands and functions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Docker for production without docker-compose:

.. code-block:: bash

   $ make bare-build # call docker build

Running werbservice:

.. code-block:: bash

   $ make bare-webservice # call docker run
