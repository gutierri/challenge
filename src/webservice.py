import os

from flask import (
    Flask,
    jsonify,
    request
)
from flask_jwt_extended import (
    JWTManager,
    jwt_required,
    create_access_token
)

from .rss2json import FeedToJson


app = Flask(__name__)

app.config.update(dict(
    JWT_SECRET_KEY=os.environ.get('JWT_SECRET_KEY', 'jwt development key')
))

jwt = JWTManager(app)


@app.route('/')
@jwt_required
def index():
    feed_crawler = FeedToJson('https://revistaautoesporte.globo.com'
                              '/rss/ultimas/feed.xml')
    return jsonify(feed_crawler.feed_items()), 200


@app.route('/auth', methods=['POST'])
def auth():
    username, password = (request.json.get('username', None),
                          request.json.get('password', None))

    if username != 'test' or password != 'test':
        return jsonify({"msg": "Bad username or password"}), 401

    access_token = create_access_token(identity=username)
    return jsonify(access_token=access_token), 200
