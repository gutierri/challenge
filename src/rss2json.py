import json
from collections import OrderedDict

import feedparser
from parsel import Selector


class FeedToJson:
    def __init__(self, feed_url):
        self.feed_url = feed_url

    def connect_exservice(self):
        return feedparser.parse(self.feed_url)

    def _filter_content(self, html_content_feed):
        sel = Selector(text=html_content_feed).css('p, img, ul')
        case_filter = {'p': ('text', './/text()'),
                       'img': ('image', '@src'),
                       'ul': ('links', '//li//a/@href')}
        content = []
        for html_tag in sel:
            content_type, query_selector = case_filter[html_tag.root.tag]
            content_selector = html_tag.xpath(query_selector).getall()

            if html_tag.root.tag != 'ul':
                content_selector = ''.join(content_selector).strip()

            if not content_selector:
                continue

            content.append(dict(content=content_selector, type=content_type))

        return content

    def parse_item(self, feed_item):
        return OrderedDict(zip(('title', 'link', 'content'),
                               (feed_item['title'], feed_item['link'],
                                self._filter_content(feed_item['summary']))))

    def feed_items(self):
        feed_items = self.connect_exservice().entries
        return dict(feed=[self.parse_item(e) for e in feed_items])


if __name__ == '__main__':
    feed_crawler = FeedToJson('https://revistaautoesporte.globo.com/'
                              'rss/ultimas/feed.xml')
    print(json.dumps(feed_crawler.feed_items(), indent=4))
