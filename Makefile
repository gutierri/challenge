bare-build:
	docker build -t challenge-globo .

bare-webservice:
	docker run --rm -it -p 5000:5000 challenge-globo

bare-feed:
	docker run --rm -it -p 5000:5000 challenge-globo python -m src.rss2json

build:
	docker-compose build

feed:
	docker-compose run feed

webservice:
	docker-compose run webservice

tests:
	python -m "unittest"

all-tests:
	tox

.PHONY: tests all-tests build webservice feed bare-build bare-webservice
