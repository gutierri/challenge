FROM python:3.5

WORKDIR /opt/feedtojson/

COPY requirements.txt /opt/feedtojson/

COPY ./src/ /opt/feedtojson/src/

RUN pip install -r requirements.txt

ENV FLASK_APP=src/webservice.py
ENV FLASK_ENV=production

CMD ["flask", "run", "--host", "0.0.0.0"]
